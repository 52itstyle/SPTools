## 小七 SmartShop 商城源码，开源代码在更新中


##### 交流微信

![输入图片说明](%E5%9F%BA%E7%A1%80%E7%8E%AF%E5%A2%83/e60927a508ba3ac9c744199da5cfb82a.jpg)


### 商城介绍
**体验地址**：由于每周更新密码，请添加微信号：kk5202023

小七商城系统支持商家入驻，后端基于SpringBoot 研发，前端使用 Vue、uniapp开发， **系统全端全部代码开源**

前后端分离，支持分布式部署，支持Docker，各个API独立，并且有独立的消费者。

### 商城页面展示

![输入图片说明](%E5%9F%BA%E7%A1%80%E7%8E%AF%E5%A2%83/251290c78cd4aace1e27353901d7bbba_origin.png)

![输入图片说明](%E5%9F%BA%E7%A1%80%E7%8E%AF%E5%A2%83/993437291df6b9a8cc097456a160f2b4_origin.png)

### 系统功能
移动端支持DIY：22种组件；
多应用端展示：公众号端、H5端、小程序端、APP端、PC端（需要购买）；
多种消息通知：公众号模版、小程序模版、短信、打印机
对外接口：包含用户、商品、订单等接口，方便其它系统对接；
多种云储存：阿里云、腾讯云、华为云、七牛云、MINIO、本地


### 商城功能

多种支付方式：微信支付、支付宝支持；
多种商品类型：标准商品、虚拟商品、优惠券商品；
多种营销功能：拼团、砍价、秒杀、优惠券、积分、经验、分销、充值、签到、渠道码；
多种运费方式：快递、配送、自提、包邮、超强运费模版管理、多自提点

### 商城简介

小七商城是一款持续更新得轻量级、高性能、前后端分离的电商系统，包含 小程序 、 APP 、 H5 、 PC 等多终端。我们具有多种商业模式满足你对商城源码得各种需求，其中有 S2B2C供应链商城、B2B2C多商户商城、B2C单商户商城、O2O外卖商城、社区团购 等多种商业模式并且含有 装修模板、分账、用户等级、会员、直播、秒杀、优惠卷、拼团、同城、满减 等一系列特色商城功能，还有更多DIY功能等你开发

#### 卖家功能


| 模块 <img width=80/> | 功能                            |
|----|-------------------------------| 
| 首页 | 店铺基础信息统计、待办事项、店铺公告            |
| 商品 | 商品发布、商品列表、商品模板、店铺分类           |
| 订单 | 商品订单、虚拟订单、订单评价、订单投诉、退款申请、退货申请 |
| 财务 | 店铺对账、店铺结算、发票管理                |
| 促销 | 优惠券、满额优惠、秒杀、拼团 、分销商品     |
| 统计 |单统计、流量统计、商品销量统计                                         |
| 设置 | 配送公司、物流模板、店铺设置、店铺自提设置、PC装修、移动端装修、店员管理、部门管理、角色管理 |
| 消息 | 站内信 |


### 商业授权

商业使用需要授权，授权方式请联系微信。

商业授权模式为永久授权，支持永久升级。


### 开源须知
1.开源项目供学习使用，跑不起来可以联系微信咨询
2.如果需要商业使用请联系我们
<div align="center">
  <h2>SPAdmin</h2>
  <a href="#" target="_blank">
      <img src="https://images.gitee.com/uploads/images/2021/0224/195754_b55ad391_87650.png" width="200">
  </a>
</div>

#### 介绍

市面上权限框架大多都是采用SpringBoot、MyBatis、Shiro的居多，这里来一个SpringBoot、JPA、Shiro的。

#### 基础环境

JDK1.8、Maven、Mysql、Redis、IntelliJ IDEA、minio、fastdfs、OpenOffice

#### 分支版本

- [正式版](https://gitee.com/52itstyle/SPTools)
- [网盘版](https://gitee.com/52itstyle/SPTools/tree/%E7%BD%91%E7%9B%98%E7%89%88/)
- [纯净版](https://gitee.com/52itstyle/SPTools/tree/%E7%BA%AF%E5%87%80%E7%89%88/)
- [博客版](https://gitee.com/52itstyle/SPTools/tree/%E5%8D%9A%E5%AE%A2%E7%89%88/)


#### 妹子图

[对，就是那个大家经常爬的妹子图！](https://gitee.com/52itstyle/mzitu)


#### 相关组件

- [ok-admin](https://gitee.com/bobi1234/ok-admin)
- [vue](https://cn.vuejs.org/)
- [iView](http://v1.iviewui.com/)
- [echarts](https://echarts.apache.org/zh/index.html)
- clipboard
- cropperjs
- lightbox
- nprogress
- webuploader
- ztree

#### 内置功能

- 组织机构：机构管理、用户管理、角色管理、行政区域。

- 系统监控：系统日志、在线用户，后期会慢慢追加完善。

- 应用管理：任务调度、邮件管理、图片管理、文章管理、人工智能，每个模块只需要你稍作修改就可以打造成一个项目了。

- 工作流：一个简单的工作流功能，后期慢慢完善。

- 文档管理：文档上传、转换、预览功能，后期慢慢完善。

- 系统管理：敏捷开发、系统菜单、全局配置、在线代码编辑器，小伙伴们只需要设计好表结构，三秒中就能撸出一个增删查改的模块。


#### 推荐阅读


- [深夜吐血训练了100万小xx xx了一个xx接口](https://blog.52itstyle.vip/archives/4863/)

- [UCloud 云服务内容xx Java 版本实现](https://blog.52itstyle.vip/archives/4935/)

- [分享一款炒鸡好用的网盘+文件服务器](https://blog.52itstyle.vip/archives/5275/)

- [SpringBoot 2.x 开发案例xx图接入 Redis 缓存](https://blog.52itstyle.vip/archives/5177/)

- [SpringBoot 2.x 开发案例之 Shiro 整合 Redis](https://blog.52itstyle.vip/archives/5092/)

- [SpringBoot 2.x 开发案例之优雅的处理异常](https://blog.52itstyle.vip/archives/5069/)

- [SpringBoot 2.0 开发案例之整合FastDFS分布式文件系统](https://blog.52itstyle.vip/archives/4837/)



#### 安装教程

- 启动前请配置 `application-dev.properties` 中相关`mysql`、`redis`以及非启动强依赖配置邮件、鉴黄、阿里云存储、分布式文件存储。

- 数据库脚本位于企鹅群 `933593697`共享文件`炒鸡工具箱基础语句`，启动前请自行导入。

- 请自行启动 `redis`，见基础环境目录，里面有配置教程。

- 请自行安装 `OpenOffice`，文件过大，不易上传，见群文件。

- 配置完成，运行`Application`中的 `main `方法。

- 测试账号：admin 密码：admin2020

- 最后如果不想安装各种依赖，请切换分支到纯净版

#### 炒鸡工具箱交流群

企鹅群：933593697

![输入图片说明](https://images.gitee.com/uploads/images/2020/0521/174508_3fc74b80_87650.png "超级工具箱群二维码.png")

#### 推荐


- 秒杀案例：https://gitee.com/52itstyle/spring-boot-seckill

- 任务调度：https://gitee.com/52itstyle/spring-boot-quartz

- 邮件服务：https://gitee.com/52itstyle/spring-boot-mail

- 搜索服务：https://gitee.com/52itstyle/spring-boot-elasticsearch

- 三大支付：https://gitee.com/52itstyle/spring-boot-pay


作者： 小柒2012

欢迎关注： https://blog.52itstyle.vip
